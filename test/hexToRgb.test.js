// test/hexToRgb.test.js

const chai = require("chai");
const expect = chai.expect;
const hexToRgb = require("../hexToRgb"); // Update the path as needed

describe("hexToRgb", () => {
  it("should convert a valid HEX code to RGB", () => {
    const hex = "#FF5733";
    const rgb = hexToRgb(hex);
    expect(rgb).to.deep.equal({ r: 255, g: 87, b: 51 });
  });

  it("should handle a different valid HEX code", () => {
    const hex = "#00FF00";
    const rgb = hexToRgb(hex);
    expect(rgb).to.deep.equal({ r: 0, g: 255, b: 0 });
  });
});

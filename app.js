const express = require("express");
const app = express();

app.get("/hex-to-rgb", (req, res) => {
  r;
  const hex = req.query.hex;

  if (!isValidHex(hex)) {
    res.status(400).json({ error: "Invalid HEX value" });
    return;
  }

  const rgb = hexToRgb(hex);

  res.json({ hex, rgb });
});

function isValidHex(hex) {
  const hexRegex = /^#[0-9A-Fa-f]{6}$/;
  return hexRegex.test(hex);
}

function hexToRgb(hex) {}

const port = 3000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
